# nozama-system-backend

This is the backend for Nozama system. It provides API's for data access and user authentication.

## Building & Running
### Prerequisites
- Rust (cargo)
- diesel_cli (`cargo install diesel_cli`)

### Prepare the environment
1. Create the database
```shell
diesel setup
```

2. Create database structure
```shell
diesel migration run
```

### Building
1. Build the project
```shell
cargo build --package system-backend --release
```


### Run
```shell
./target/system-backend
```