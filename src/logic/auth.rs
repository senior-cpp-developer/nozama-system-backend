use diesel::prelude::*;
use crate::database::diesel::{DbPool, establish_connection, get_from_pool};

use crate::database::models::users::*;
use crate::database::schema::users::dsl::*;
use crate::database::schema::sessions::dsl::*;
use crate::database::schema::sessions::dsl::{user_id, token, isValid};

pub fn auth_user(dbpool: &DbPool, user_token: &String) -> bool {
    let connection = get_from_pool(dbpool).unwrap();

    let results = sessions.filter(token.eq_all(user_token))
        .select((user_id, token, isValid))
        .filter(isValid.eq_all(true))
        .load::<Session>(&connection).unwrap();
    if results.len() == 1 {
        return true;
    }
    false
}