use diesel::{insert_into, update};
use rocket::serde::json::Json;
use rocket::serde::{Deserialize, Serialize};
use rocket::{Response, State};
use diesel::prelude::*;
use rocket::http::Status;
use crate::database::diesel::{DbPool, establish_connection, get_from_pool};

use crate::database::models::users::*;
use crate::database::schema::users::dsl::*;
use crate::database::schema::sessions::dsl::*;
use crate::database::schema::sessions::dsl::{user_id, token, isValid};

use rand::{Rng, RngCore};
use rand::distributions::Alphanumeric;
use rand::rngs::OsRng;

#[post("/")]
pub fn user_index() -> &'static str {
    "{\r\n  \"name\": \"Angery boi\",\r\n  \"description\": \"He's boi and he's angery\",\r\n  \"properties\": [\r\n    \"price\",\r\n    \"is-angery\"\r\n  ],\r\n  \"price\": 6969,\r\n  \"is-angery\": true,\r\n  \"images\": [\r\n    \"https://i.ytimg.com/vi/iik25wqIuFo/maxresdefault.jpg\"\r\n  ]\r\n}"
}

#[derive(Deserialize)]
pub struct UserLoginData {
    auth_secret: String,
    login: String,
    password: String
}

#[derive(Serialize)]
pub struct UserLoginResponse {
    user_token: String
}

#[post("/login", format = "application/json", data = "<data>")]
pub fn user_login(dbpool: &State<DbPool>, data: Json<UserLoginData>)
    -> Result<Json<UserLoginResponse>, Status> {
    let connection = get_from_pool(dbpool).unwrap();

    let results = users.filter(email.eq_all(&data.login))
        .filter(psswd_hash.eq_all(&data.password))
        .load::<User>(&connection).unwrap();
    if results.len() != 1 {
        return Err(Status::Unauthorized);
    }
    let user = results.get(0).unwrap();

    let rnd: String = OsRng
        .sample_iter(&Alphanumeric)
        .take(30)
        .map(char::from)
        .collect();
    let new_session = Session {
        user_id: user.user_id,
        token: rnd.clone(),
        isValid: true
    };
    insert_into(sessions)
        .values(new_session)
        .execute(&connection);

    let response = UserLoginResponse {
        user_token: rnd,
    };
    Ok(Json(response))
}

#[derive(Deserialize)]
pub struct UserLogoutData {
    auth_secret: String,
    user_token: String,
}

#[derive(Serialize)]
pub struct UserLogoutResponse {
}

#[post("/logout", format = "application/json", data = "<data>")]
pub fn user_logout(dbpool: &State<DbPool>, data: Json<UserLogoutData>)
    -> Result<Json<UserLogoutResponse>, Status> {

    let connection = get_from_pool(dbpool).unwrap();
    let results = sessions.filter(token.eq_all(&data.user_token))
        .select((user_id, token, isValid))
        .load::<Session>(&connection).unwrap();
    if results.len() == 0 {
        return Err(Status::Unauthorized);
    }
    if results.len() != 1 {
        return Err(Status::InternalServerError);
    }
    let session = results.get(0).unwrap();
    if !session.isValid {
        return Err(Status::Unauthorized);
    }


    update(sessions.filter(token.eq_all(&data.user_token)))
        .set(isValid.eq_all(false))
        .execute(&connection);

    Ok(Json(UserLogoutResponse{}))
}