use diesel::insert_into;
use rocket::serde::json::Json;
use rocket::serde::{Deserialize, Serialize};
use rocket::{Response, State};
use diesel::prelude::*;
use rocket::http::Status;
use crate::database::diesel::{DbPool, establish_connection, get_from_pool};

use crate::database::models::db::*;
use crate::database::schema::items::dsl::{items, id, name, description, image_link};
use crate::database::schema::entities::dsl::*;

use crate::logic::auth::auth_user;

#[derive(Deserialize)]
pub struct EntityGetData {
    auth_secret: String,
    user_token: String,
    item_id: i32,
    item_ins: i32
}

#[derive(Serialize)]
pub struct EntityGetResponse {
    name: String,
    description: String,
    image_link: String,
    amount: i32,
    price: f64,
    localization: i32
}

#[post("/<db_name>/entity/get", format = "application/json", data = "<data>")]
pub fn entity_get(dbpool: &State<DbPool>, db_name: String, data: Json<EntityGetData>)
    -> Result<Json<EntityGetResponse>, Status> {

    if !auth_user(dbpool, &data.user_token) {
        return Err(Status::Unauthorized);
    }
    let connection = get_from_pool(dbpool).unwrap();

    let results = items.inner_join(entities.on(id.eq(id)))
        .select((id, name, description, image_link,
            ins, amount, price, localization))
        .filter(id.eq(data.item_id))
        .filter(ins.eq(data.item_ins))
        .load::<Entity>(&connection).unwrap();
    if results.len() == 0 {
        return Err(Status::NotFound);
    }
    if results.len() != 1 {
        return Err(Status::InternalServerError);
    }
    let entity = results.get(0).unwrap();

    let response = EntityGetResponse {
        name: entity.name.clone(),
        description: entity.description.clone().unwrap(),
        image_link: entity.image_link.clone().unwrap(),
        amount: entity.amount,
        price: entity.price.unwrap(),
        localization: entity.localization.unwrap()
    };
    Ok(Json(response))
}