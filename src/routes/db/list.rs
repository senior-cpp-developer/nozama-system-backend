use diesel::insert_into;
use rocket::serde::json::Json;
use rocket::serde::{Deserialize, Serialize};
use rocket::{Response, State};
use diesel::prelude::*;
use rocket::http::Status;
use crate::database::diesel::{DbPool, establish_connection, get_from_pool};

use crate::database::models::db::*;
use crate::database::schema::items::dsl::{items, id, name, description, image_link};
use crate::database::schema::entities::dsl::*;
use crate::database::schema::entities;
use crate::get::EntityGetResponse;

use crate::logic::auth::auth_user;

use diesel::result::Error;

#[derive(Deserialize)]
pub struct ListData {
    auth_secret: String,
    user_token: String,
    search: i32,
}

#[derive(Serialize)]
pub struct ListResponse {
    entities: Vec<ListField>
}

#[derive(Serialize)]
pub struct ListField {
    item_id: i32,
    item_ins: i32,
    name: String,
    image_link: String,
}

#[post("/<db_name>/entity/list", format = "application/json", data = "<data>")]
pub fn list(dbpool: &State<DbPool>, db_name: String, data: Json<ListData>)
                  -> Result<Json<ListResponse>, Status> {

    if !auth_user(dbpool, &data.user_token) {
        return Err(Status::Unauthorized);
    }
    let connection = get_from_pool(dbpool).unwrap();

    let mut results: Vec<Entity> = Vec::new();
    if data.search == 0 {
        results = items.inner_join(entities.on(id.eq(entities::dsl::id)))
            .select((id, name, description, image_link,
                     ins, amount, price, localization))
            .load::<Entity>(&connection).unwrap();
    }
    else {
        results = items.inner_join(entities.on(id.eq(entities::dsl::id)))
            .select((id, name, description, image_link,
                     ins, amount, price, localization))
            .filter(id.eq(data.search))
            .load::<Entity>(&connection).unwrap();
    }

    let mut vector2: Vec<ListField> = Vec::new();
    for entity in results {
        vector2.push(ListField{
            item_id: entity.id,
            item_ins: entity.ins,
            name: entity.name.clone(),
            image_link: entity.image_link.clone().unwrap(),
        })
    }
    Ok(Json(ListResponse{
        entities: vector2,
    }))
}