use diesel::insert_into;
use rocket::serde::json::Json;
use rocket::serde::{Deserialize, Serialize};
use rocket::{Response, State};
use diesel::prelude::*;
use rocket::http::Status;
use serde::de::Unexpected::Option;
use crate::database::diesel::{DbPool, establish_connection, get_from_pool};

use crate::database::models::db::*;
use crate::database::schema::items::dsl::{items, id, name, description, image_link};
use crate::database::schema::entities::dsl::*;

use crate::logic::auth::auth_user;


#[derive(Deserialize)]
pub struct EntityCreateData {
    auth_secret: String,
    user_token: String,
    name: String,
    description: String,
    image_link: String,
    amount: i32,
    price: f64,
    localization: i32,
}

#[derive(Serialize)]
pub struct EntityCreateResponse {
}


#[post("/<db_name>/entity/create", format = "application/json", data = "<data>")]
pub fn entity_create(dbpool: &State<DbPool>, db_name: String, data: Json<EntityCreateData>)
    -> Result<Json<EntityCreateResponse>, Status> {

    if !auth_user(dbpool, &data.user_token) {
        return Err(Status::Unauthorized);
    }
    let connection = get_from_pool(dbpool).unwrap();
    insert_into(items).values(
        ItemCreate{
            name: data.name.clone(),
            description: Some(data.description.clone()),
            image_link: Some(data.image_link.clone()),
    }).execute(&connection);
    let added = items.filter(name.eq_all(&data.name))
        .load::<Item>(&connection).unwrap();
    let item = added.get(0).unwrap();

    insert_into(entities).values(
        EntityCreate{
            id: item.id,
            amount: data.amount,
            price: Some(data.price),
            localization: Some(data.localization),
        }
    ).execute(&connection);

    Ok(Json(EntityCreateResponse{}))
}