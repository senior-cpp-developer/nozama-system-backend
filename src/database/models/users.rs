use diesel::sql_types::Date;
use crate::database::schema::*;

#[derive(Queryable)]
pub struct User {
    pub user_id: i32,
    pub name: String,
    pub description: Option<String>,
    pub avatar_link: Option<String>,
    pub passwd_hash: String,
    pub email: String,
}

#[derive(Queryable)]
#[derive(Insertable)]
#[table_name="sessions"]
pub struct Session {
    pub user_id: i32,
    pub token: String,
    pub isValid: bool,
}

#[derive(Queryable)]
pub struct SessionAll {
    pub user_id: i32,
    pub token: String,
    pub login_date: Date,
    pub isValid: bool,
}