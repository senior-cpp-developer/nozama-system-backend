use diesel::sql_types::{Double, Nullable};
use crate::database::schema::*;

#[derive(Queryable)]
pub struct Entity {
    pub id: i32,
    pub name: String,
    pub description: Option<String>,
    pub image_link: Option<String>,
    pub ins: i32,
    pub amount: i32,
    pub price: Option<f64>,
    pub localization: Option<i32>,
}

#[derive(Insertable)]
#[table_name="items"]
pub struct ItemCreate {
    pub name: String,
    pub description: Option<String>,
    pub image_link: Option<String>,
}

#[derive(Insertable)]
#[table_name="entities"]
pub struct EntityCreate {
    pub id: i32,
    pub amount: i32,
    pub price: Option<f64>,
    pub localization: Option<i32>,
}

#[derive(Queryable)]
pub struct Item {
    pub id: i32,
    pub name: String,
    pub description: Option<String>,
    pub image_link: Option<String>,
}

#[derive(Queryable)]
pub struct Instance {
    pub ins: i32,
    pub amount: i32,
    pub price: Option<f64>,
    pub localization: Option<i32>,
}