table! {
    entities (ins) {
        id -> Integer,
        ins -> Integer,
        amount -> Integer,
        price -> Nullable<Double>,
        localization -> Nullable<Integer>,
    }
}

table! {
    items (id) {
        id -> Integer,
        name -> Text,
        description -> Nullable<Text>,
        image_link -> Nullable<Text>,
    }
}

table! {
    sessions (token) {
        user_id -> Integer,
        token -> Text,
        login_date -> Date,
        isValid -> Bool,
    }
}

table! {
    users (user_id) {
        user_id -> Integer,
        name -> Text,
        description -> Nullable<Text>,
        avatar_link -> Nullable<Text>,
        psswd_hash -> Text,
        email -> Text,
    }
}

allow_tables_to_appear_in_same_query!(
    entities,
    items,
    sessions,
    users,
);
