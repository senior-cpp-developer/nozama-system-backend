mod routes;
mod database;
mod logic;

#[macro_use]
extern crate rocket;
#[macro_use]
extern crate diesel;

use diesel::r2d2::{ConnectionManager, PooledConnection};
use diesel::SqliteConnection;
use routes::user::*;
use routes::db::*;
use crate::database::diesel::establish_pooled_connection;


#[launch]
fn rocket() -> _ {
    rocket::build().mount("/", routes![index])
        .mount("/user", routes![user_index, user_login, user_logout])
        .mount("/", routes![get::entity_get, list::list, create::entity_create])
        .manage(establish_pooled_connection())
}

#[post("/")]
fn index() -> &'static str {
    "{\r\n  \"name\": \"Angery boi\",\r\n  \"description\": \"He's boi and he's angery\",\r\n  \"properties\": [\r\n    \"price\",\r\n    \"is-angery\"\r\n  ],\r\n  \"price\": 6969,\r\n  \"is-angery\": true,\r\n  \"images\": [\r\n    \"https://i.ytimg.com/vi/iik25wqIuFo/maxresdefault.jpg\"\r\n  ]\r\n}"
}
