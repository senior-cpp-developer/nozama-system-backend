create table users
(
    user_id     integer      not null
        constraint users_pk
            primary key autoincrement,
    name        varchar(32)  not null,
    description varchar(256),
    avatar_link varchar(256),
    psswd_hash  varchar(256) not null,
    email       varchar(64)  not null
);

create unique index users_email_uindex
    on users (email);

create unique index users_user_id_uindex
    on users (user_id);