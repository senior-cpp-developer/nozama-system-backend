create table sessions
(
    user_id    integer     not null,
    token      varchar(32) not null
        constraint sessions_pk
            primary key,
    login_date date default CURRENT_TIMESTAMP not null,
    isValid    BOOLEAN     not null
);

create unique index sessions_token_uindex
    on sessions (token);

