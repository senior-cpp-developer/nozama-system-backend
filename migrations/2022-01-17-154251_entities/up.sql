create table entities
(
    id           integer not null,
    ins          integer not null
        constraint instances_pk
            primary key autoincrement,
    amount       integer not null,
    price        numeric(7, 2),
    localization integer
);

create unique index entities_ins_uindex
    on entities (ins);-- Your SQL goes here