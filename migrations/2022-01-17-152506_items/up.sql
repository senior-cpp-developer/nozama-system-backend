create table items
(
    id          integer     not null
        constraint items_pk
            primary key autoincrement,
    name        varchar(32) not null,
    description varchar(256),
    image_link  varchar(256)
);

create unique index items_id_uindex
    on items (id);

create unique index items_name_uindex
    on items (name);